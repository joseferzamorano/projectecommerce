var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var watch = require('gulp-watch');
var gutil = require('gulp-util');
var browserify = require('browserify');
var babelify = require('babelify');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');

//var server = require('gulp-webserver');
var browserSync = require('browser-sync').create(); // create a browser sync instance.
var sass = require('gulp-sass'); //compiles SASS
var minifyCss = require('gulp-minify-css'); //minifies css
var concat = require('gulp-concat'); //joins multiple files into one
var rename = require('gulp-rename');
var merge = require('merge-stream');


var path ={
  SOURCE_DIR:'./src/',
  SOURCE_MAIN_JSX_FILE:'./src/frameworks/mainFile.jsx',
  SOURCE_GENERIC_JS:'./src/js/**/',
  SOURCE_GENERIC_SCSS:'./src/scss/**/',
  SOURCE_SCSS:'./src/scss/styles_importer.scss',
  DESTINATION_JS_DIR:'./dist/js/',
  DESTINATION_CSS_DIR:'./dist/css/',
  DESTINATION_DIR:'./dist/',
}

gulp.task('browserSync',function(){
      browserSync.init({
         server: {
             baseDir: "./"
         }
     });
})

gulp.task('transformSass', function(){
    return gulp.src(path.SOURCE_SCSS)
      .pipe(sass().on('error', sass.logError))
      .pipe(concat('style.css'))
      .pipe(minifyCss())
      .pipe(rename({              //renames the concatenated CSS file
          basename : 'style',       //the base name of the renamed CSS file
          extname : '.min.css'      //the extension fo the renamed CSS file
        }))
      .pipe(gulp.dest(path.DESTINATION_CSS_DIR))
});

//MANUAL WATCH NO BROWSER RELOAD end========
gulp.task('transformJsx', function() {
    return browserify({
            entries: path.SOURCE_MAIN_JSX_FILE,
            debug: true,
            extensions:['.js','.jsx','.json']
        })
        .transform('babelify',{
            presets: ["react", "es2015",'stage-0']
        })
        .bundle().on('error', gutil.log)
        .pipe(source('mainReact.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps:true}))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(path.DESTINATION_JS_DIR))
});

gulp.task('transformJs',function(){
    return gulp.src(path.SOURCE_GENERIC_JS)
        .pipe(concat('scripts.js'))
        .pipe(rename({
          basename:'script',
          extname:'.min.js'
        }))
        .pipe(gulp.dest(path.DESTINATION_JS_DIR))
});

gulp.task('transformJsxAndJs',function(){
    var jsxStream = browserify({
                        entries: path.SOURCE_MAIN_JSX_FILE,
                        debug: true,
                        extensions:['.js','.jsx','.json']
                    })
                    .transform('babelify',{
                        presets: ["react", "es2015",'stage-0']
                    })
                    .bundle().on('error', gutil.log)
                    .pipe(source('main.js'))
                    .pipe(buffer());

    var jsStream = gulp.src(path.SOURCE_GENERIC_JS)
                   .pipe(concat('script.js'));

    var mergedAndConcatenatedJs = merge(jsStream,jsxStream)
                   .pipe(concat('main.js'))
                   .pipe(sourcemaps.init({loadMaps:true}))
                   .pipe(sourcemaps.write('.'))
                   .pipe(gulp.dest(path.DESTINATION_JS_DIR));

    return mergedAndConcatenatedJs;
})

gulp.task('watchScssFiles',function(){
    gutil.log('watchScssFiles is running');
    return gulp.watch([path.SOURCE_GENERIC_SCSS,'src/scss/**/*.*','src/scss/**'],['transformSass']);
});

gulp.task('watchJsxSourceFile',function(){
   gutil.log('watchJsxSourceFile is running');
    return gulp.watch([path.SOURCE_MAIN_JSX_FILE,'./src/frameworks/**'], ['transformJsx']);
});

gulp.task('watchJsFiles',function(){
   gutil.log('watchJsFiles is running');
    return gulp.watch('./src/js/**', ['transformJs']);
});

gulp.task('watchJsxAndJsFiles',function(){
    gutil.log('watchJsxAndJsFiles is running');
    return gulp.watch(['./src/js/**','./src/js/**/*.*','./src/frameworks/**','./src/frameworks/**/*.*'],['transformJsxAndJs']);
});

//MANUAL WATCH NO BROWSER RELOAD end========



// TASKS for Browser Reload after task is completed
// reloading the browser
gulp.task('transformSassThenReload', ['transformSass'], function (done) {
    gutil.log('browserReload');
    browserSync.reload();
    done();
});

gulp.task('transformJsxThenReload', ['transformJsx'], function (done) {
    gutil.log('browserReload');
    browserSync.reload();
    done();
});

gulp.task('transformJsThenReload', ['transformJs'], function (done) {
    gutil.log('browserReload');
    browserSync.reload();
    done();
});

//TASK to watch files
gulp.task('watchScssFilesAndReload',function(){
    gutil.log('watchScssFiles is running');
    return gulp.watch([path.SOURCE_GENERIC_SCSS,'src/scss/**/*.*','src/scss/**'],['transformSassThenReload']);
});

gulp.task('watchJsxSourceFileAndReload',function(){
   gutil.log('watchJsxSourceFile is running');
    return gulp.watch([path.SOURCE_MAIN_JSX_FILE,'./src/frameworks/**'], ['transformJsxThenReload']);

});

gulp.task('watchJsFilesAndReload',function(){
   gutil.log('watchJsFiles is running');
    return gulp.watch('./src/js/**', ['transformJsThenReload']);
});

//Watch Files Task
gulp.task('watch',
          [
             'browserSync',
             'watchScssFilesAndReload',
             'watchJsxSourceFileAndReload',
             'watchJsFilesAndReload']
          );

/*************BUILD FOR PRODUCTION********/
gulp.task('build',['transformSass','transformJsx','transformJs']);
