var heroCarousel = document.getElementsByClassName('hero-carousel');

var chevronLeft = document.getElementsByClassName('chevron-left');
var chevronRight = document.getElementsByClassName('chevron-right');

var timeIntervalToExecuteNextSlide = 10000 //ten seconds
var standardSpeed = 1000;
var carouselIsAnimating = false;

if(chevronLeft.length>0 && chevronLeft!=null && chevronLeft !='undefined'){
	chevronLeft[0].addEventListener('click',reverseCarousel)
}

if(chevronRight.length>0){
	chevronRight[0].addEventListener('click',accelerateCarousel);
}

if(heroCarousel.length>0 && heroCarousel !=null && heroCarousel != 'undefined'){
	var fileLocation = [
    './src/assets/images/ringpixel-bee-6XjrarDC97U-unsplash.jpg',
		'./src/assets/images/diamond-1186139_1920.jpg',
		'./src/assets/images/ring-heather-mount-ph3z4KuJ4OA-unsplash.jpg',
		'./src/assets/images/diamond-1199183_1920.jpg',
		'./src/assets/images/ringringkuromi-lu-_OG-hxrBSUQ-unsplash.jpg',
		'./src/assets/images/diamond-1839031_1920.jpg',
		'./src/assets/images/necklacerose-3030462_1920.jpg',
		'./src/assets/images/ringamethyst-2186842_1920.jpg',
		'./src/assets/images/diamonds-2142417_1280.jpg',
		'./src/assets/images/ringwedding-812967_1920.jpg'
	];

	var imagesArray = [];
	for(var a=0; a< fileLocation.length; a++){
		var imageElement = document.createElement("IMG");
		imageElement.src = fileLocation[a];
		imageElement.classList.add('sliding-images')
		if(a === 0){
			imageElement.classList.add('display')
		}
		else{
			imageElement.classList.add('hide')
		}
		imagesArray.push(imageElement);
	}

	for(var b= 0 ; b <imagesArray.length; b++){
		heroCarousel[0].appendChild(imagesArray[b]);
	}
}

window.onload = runFeatures;
function runFeatures(){
	setInterval(runCarousel, timeIntervalToExecuteNextSlide);
}


function accelerateCarousel(){
	if(carouselIsAnimating===true){
		return;
	}
	else{
		carouselIsAnimating = runCarousel(200);
	}
}


function runCarousel(selectedSpeed){
	carouselIsAnimating = true;
	var slidingImages =$('.sliding-images');
	var displayCounter = 0;
	for(var a=0; a<slidingImages.length; a++){
		for(var b = 0; b< slidingImages[a].classList.length ;b++){
			if(slidingImages[a].classList[b]==='display'){
				displayCounter++;
				var speed=(selectedSpeed ==null || selectedSpeed==='undefined')? standardSpeed:selectedSpeed;
				var currentActiveImageWidth = slidingImages[a].width-5;
				var leftBeyondBrowserDisplay= '-'+(currentActiveImageWidth)+'px';
				var slidingSingleImage$ = slidingImages.eq(a);
				slidingSingleImage$.css({'left':'0px'})
				.animate({left: leftBeyondBrowserDisplay},speed,function(){})
				.promise().done(function(){
					slidingSingleImage$.css({'left':'0px'});
					slidingImages[a].classList.remove('display');
				    slidingImages[a].classList.add('hide');
				});

				//we get the next image
				var nextIndex = a+1;
				if(nextIndex === slidingImages.length){
					nextIndex = 0;
        }

        slidingImages[nextIndex].classList.remove('hide');
				slidingImages[nextIndex].classList.add('display');
				slidingImages.eq(nextIndex).css({'left': currentActiveImageWidth+'px'})
				.animate({left: '0px'},speed,function(){})
        .promise().done(function(){
					carouselIsAnimating = false;
				});
				return;
			}
		}
	}

	//At the begining there is no insered class 'display'
	//so the counter equals zero
	if(displayCounter === 0){
		var initialValue = 0;
		processImageDisplay(initialValue, selectedSpeed);
	}

	function processImageDisplay( index,selectedSpeed){
		if(slidingImages.length>0){
			var speed=(selectedSpeed ==null || selectedSpeed==='undefined')? standardSpeed:selectedSpeed;
			var currentActiveImageWidth = slidingImages[index].width;
			var leftBeyondBrowserDisplay= '-'+(currentActiveImageWidth)+'px';
			var slidingSingleImage$ = slidingImages.eq(index);
			slidingSingleImage$.css({'left':'0px'})
			.animate({left: leftBeyondBrowserDisplay},speed,function(){
			})
			.promise().done(function(){
				slidingSingleImage$.css({'left':'0px'});
				slidingImages[index].classList.remove('display');
				slidingImages[index].classList.add('hide');
			});

			//we get the next image
			var nextIndex = index+1;
			if(nextIndex === slidingImages.length){
				nextIndex = 0;
			}

			slidingImages[nextIndex].classList.remove('hide');
			slidingImages[nextIndex].classList.add('display');
			slidingImages.eq(nextIndex).css({'left': currentActiveImageWidth+'px'})
			.animate({left: '0px'},speed,function(){})
			.promise().done(function(){
				carouselIsAnimating = false;
			});
			return;
		}
	}
}

function reverseCarousel(){
	if(carouselIsAnimating ===true){
		return ;
	}
	else{
		  carouselIsAnimating = runCarouselReversed(300);
	}
}

function convertStringToNumber(string, defaultNumber){
	var valueString = string.replace( /[^\d. +-]/g, '' );
	var result = parseInt(valueString);
	var valueInt = isNaN(result)? defaultNumber : result;
	return valueInt;
}

function runCarouselReversed(selectedSpeed){
  carouselIsAnimating = true;
	var slidingImages =$('.sliding-images');
	for(var a=0; a<slidingImages.length; a++){
		for(var b = 0; b< slidingImages[a].classList.length ;b++){
			if(slidingImages[a].classList[b]==='display'){
				var speed=(selectedSpeed ==null || selectedSpeed==='undefined')? standardSpeed:selectedSpeed;
				var currentActiveImageWidth = slidingImages[a].width-5;
        var slidingSingleImage$ = slidingImages.eq(a);
				var currentLeftPositionString= slidingImages[a].style.left;
				var currentLeftPosition =convertStringToNumber(currentLeftPositionString,0);
				if( currentLeftPosition < 0){
					slidingSingleImage$.css({'left':currentLeftPosition+'px'})
					.animate({left: '0px'},speed,function(){
					})
					.promise().done(function(){
						  slidingImages[a].classList.remove('hide');
					    slidingImages[a].classList.add('display');
					});

          //we get the next image
					var nextIndex = a+1;
					if(nextIndex === slidingImages.length){
						nextIndex = 0;
          }
          var nextImageLeftPositionStr = slidingImages[nextIndex].style.left;
			    var nextImageLeftPosition =convertStringToNumber(nextImageLeftPositionStr,currentActiveImageWidth);
			    var totalMoveToRight = currentActiveImageWidth + nextImageLeftPosition;
			    var slidingSingleImage$ = slidingImages.eq(nextIndex);
					slidingSingleImage$.css({'left': currentActiveImageWidth+'px'})
			    .animate({left: totalMoveToRight+'px'},speed,function(){})
			    .promise().done(function(){
			    	slidingImages[nextIndex].classList.remove('display');
			      slidingImages[nextIndex].classList.add('hide');
			    });
				}

				if( currentLeftPosition >= 0){
					slidingSingleImage$.css({'left':'0px'})
					.animate({left: currentActiveImageWidth+'px'},speed,function(){})
					.promise().done(function(){
						slidingImages[a].classList.remove('display');
					    slidingImages[a].classList.add('hide');
					});

					var previousIndex = a-1;
					if(previousIndex < 0){
						previousIndex = slidingImages.length-1;
					}
          var previousImagewidth = slidingImages[previousIndex].width;
          var initialLeftPositionPreviousImage = previousImagewidth+currentLeftPosition;
          slidingImages[previousIndex].classList.add('display');
          slidingImages[previousIndex].classList.remove('hide');
          var previousImage$ = slidingImages.eq(previousIndex);
          previousImage$
          .css({'left':'-'+initialLeftPositionPreviousImage+'px'})
          .animate({left:'0px'}, speed, function(){})
					.promise().done(function(){
						var counterDisplayCurrentImage=0;
						for(var g=0; g< slidingImages[a].classList.length; g++){
							if(slidingImages[a].classList[g]==='display'){
								counterDisplayCurrentImage++;
							}
						}
						if(counterDisplayCurrentImage>0){
							slidingImages[a].classList.add('hide');
							slidingImages[a].classList.remove('display');
						}
						carouselIsAnimating = false;
					});
				}
				return false;
			}
		}
	}
}
