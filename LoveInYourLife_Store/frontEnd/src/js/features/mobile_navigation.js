/*VARIABLES*/
var hamburger = document.querySelector('.hamburger');

/*EVENTS*/
if(hamburger !== null && hamburger !=='undefined'){
	hamburger.addEventListener('click',processFunctions);
}

window.onresize = clearFeaturesForMobile;

/*FUNCTIONS*/
function processFunctions(){
		toggleOpacity();
		togglePositioningCategories();
		togglePositioningProducts();
}

function countOccurrencesInArray(array, itemOccurrences){
	var counter = 0;
	for(var a=0; a<array.length; a++)
	{
	  if(array[a] === itemOccurrences){
			counter++
		}
  }
	return counter;
}

function insertOrRemoveElements(array, occurrences, element){
	if(occurrences===0){
			array.add(element)
	}
	if(occurrences>0) {
		array.remove(element);
	}
}

function toggleElement(array, elementOn, elementOff){
	var counterMoveDown=countOccurrencesInArray(array,elementOn);
	var counterMoveUp = countOccurrencesInArray(array,elementOff);

	if(counterMoveDown===0 && counterMoveUp===0){
		  insertOrRemoveElements(array,counterMoveDown,elementOn);
	}else{
			insertOrRemoveElements(array,counterMoveDown,elementOn);
			insertOrRemoveElements(array,counterMoveUp,elementOff);
	}
}


function togglePositioningCategories(){
  var containerCategories = document.getElementsByClassName('container-flex-categories');
	if(containerCategories !=null && containerCategories.length >0){
		  toggleElement(containerCategories[0].classList,'elementsMoveDown','elementsMoveUp')
	}
}


function togglePositioningProducts(){
  var containerstore = document.getElementsByClassName('container-store');
	if(containerstore !=null && containerstore.length >0){
		  toggleElement(containerstore[0].classList,'productsMoveDown','productsMoveUp')
	}
}

function toggleOpacity(){
	var mobileNav = document.getElementsByClassName('mobile-nav-container');
	if(mobileNav !=null && mobileNav.length >0){
		toggleElement(mobileNav[0].classList,'fadeInAndMoveDown','fadeOutAndMoveUp');
  }
}

function togglArrayElement(array,elementOn, elementOff){
  var counterOn=0;
  var counterOff = 0;
  for(var a=0; a<array.length; a++)
  {
			if(array[a]===elementOn){
				counterOn++;
			}
			if(array[a]===elementOff){
				counterOff++;
			}
	}

	if(counterOn===0 && counterOff===0){
		  array.add(elementOn)
	}

	if(counterOn>0 && counterOff===0){
		array.remove(elementOn);
		array.add(elementOff);
	}

	if(counterOn===0 && counterOff>0){
		array.add(elementOn);
		array.remove(elementOff);
	}
}

function clearFeaturesForMobile(){
	var width = (window.innerWidth > 0) ?
	             window.innerWidth : screen.width;
	if(width>768){
		var mobileNav = document.getElementsByClassName('mobile-nav-container');
		var containerCategories = document.getElementsByClassName('container-flex-categories');
		  var containerStore = document.getElementsByClassName('container-store');
		if(mobileNav !=null && mobileNav.length >0){
			mobileNav[0].classList.remove('fadeInAndMoveDown');
			mobileNav[0].classList.remove('fadeOutAndMoveUp');

			containerCategories[0].classList.remove('elementsMoveDown');
			containerCategories[0].classList.remove('elementsMoveUp');
			containerStore[0].classList.remove('productsMoveDown');
			containerStore[0].classList.remove('productsMoveUp');
		}
	}
}
