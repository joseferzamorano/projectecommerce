export function setProductStatusId(statusId){
  return {
		type: "SET_PRODUCT_STATUS_ID",
		statusId: statusId
	}
}

export function removeProductStatusId(){
  return {
		type: "REMOVE_PRODUCT_STATUS_ID",
		statusId: -1
	}
}

export function setProductDisplay(display){
  return {
		type: "SET_PRODUCT_DISPLAY",
    displayProduct:display
	}
}

export function setProductCategoryId(categoryId){
  return {
    type: "SET_PRODUCT_CATEGORY_ID",
    categoryId: categoryId
  }
}

export function removeProductCategoryId(categoryId){
  return {
    type: "REMOVE_PRODUCT_CATEGORY_ID",
    categoryId: -1
  }
}

export function businessStoreSetDataLoad(isLoaded){
  return{
    type: "BUSINESS_STORE_SET_DATA_LOAD",
    dataLoaded: isLoaded
  }
}

export function businesStoreSetProductLoad(productsArray){
  return {
    type: "BUSINESS_STORE_SET_PRODUCT_LOAD",
    infoProducts: productsArray
  }
}
