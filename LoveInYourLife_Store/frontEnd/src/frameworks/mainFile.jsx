import React from 'react';
import ReactDOM from 'react-dom';

import TransparentBackgroundPage from './modules/ux/mainDisplay/TransparentBackgroundPage.jsx';
import IndexStore from './modules/ux/mainDisplay/IndexStore.jsx';
import SingleProduct from './modules/ux/mainDisplay/midSection/components/SingleProduct.jsx';

import store from './store/index.js';
import Provider from 'react-redux';

var root = document.getElementById('root');
var firstDom = function(){
  var promise = new Promise(function(resolve, reject){
    ReactDOM.render(  <IndexStore/>,root);
    resolve('index-dom-Created')
  });
  return promise;
}

var overlay = document.getElementById('overlay');
var secondDom = function(){
  var promise = new Promise(function(resolve, reject){
    ReactDOM.render(<TransparentBackgroundPage />,overlay);
    resolve('overlay-dom-Created');
  });
  return promise;
}

var singleWindow = document.getElementById('single-window');
var thirdDom = function(){
  var promise = new Promise(function(resolve, reject){
    ReactDOM.render(  <SingleProduct />  ,singleWindow);
    resolve('singleWindow-dom-Created');
  });
  return promise;
}

firstDom().then(secondDom).then(thirdDom);

store.subscribe(()=> firstDom());
store.subscribe(()=> secondDom());
store.subscribe(()=> thirdDom());
