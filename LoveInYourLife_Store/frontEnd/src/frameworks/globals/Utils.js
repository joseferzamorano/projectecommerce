import React from 'react';

import ReactDOM from 'react-dom';
import AboutUs from '../modules/ux/mainDisplay/footer/components/AboutUs.jsx';
import TermsAndConditions from '../modules/ux/mainDisplay/footer/components/TermsAndConditions.jsx';
import ShippingsAndReturns from '../modules/ux/mainDisplay/footer/components/ShippingsAndReturns.jsx';
import PrivacyPolicy from '../modules/ux/mainDisplay/footer/components/PrivacyPolicy.jsx';
import Security from '../modules/ux/mainDisplay/footer/components/Security.jsx';

var singleProductObject = {};

export function setSingleProductObject(obj){
  singleProductObject = obj;
}

export function getSingleProductoObject(){
  return singleProductObject;
}

export function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop)){
          return false;
        }
    }
    return true;
}

export function addEventListenerForWindowResizeParams(selectedClassName) {
    window.addEventListener("resize", function(){
      handleDivResizeParams(selectedClassName);
    });
}

export function removeEventListenerForWindowResizeParams(selectedClassName) {
  window.removeEventListener("resize", function(){
    handleDivResizeParams(selectedClassName);
  });
}

export function handleDivResizeParams(selectedClassName){
  var windowDisplay = document.getElementsByClassName(selectedClassName);
  var currentSize = getBrowserCurrentSize();
  if(windowDisplay.length>0 && windowDisplay != undefined){
    windowDisplay[0].style.height = ""+currentSize.height+"px";
    windowDisplay[0].style.width = ""+currentSize.width+"px";
  }
}

export function getBrowserCurrentSize(){
  var root = document.getElementById('root');
  var size = {
    width: (window.innerWidth > document.body.clientWidth)? window.innerWidth : document.body.clientWidth,
    height: (window.innerHeight > document.body.clientHeight)? window.innerHeight : document.body.clientHeight
  }
  return size;
}

export function getSelectedComponent(selector){
  switch(selector){
    case "about_us":
      ReactDOM.render(<div><AboutUs/></div>,manifest)
    return ;

    case "t_and_c":
      ReactDOM.render(<div><TermsAndConditions/></div>,manifest)
      return ;

    case "s_and_r":
      ReactDOM.render(<div><ShippingsAndReturns/></div>,manifest)
      return ;

    case "privacy_policy":
      ReactDOM.render(<div><PrivacyPolicy/></div>,manifest)
      return ;s

      case "security":
        ReactDOM.render(<div><Security/></div>,manifest)
        return ;
  }
}

export function unmountComponent(idName){
  ReactDOM.unmountComponentAtNode(document.getElementById(idName));
}

export function closeWebPage(){
  var selectedClassName = 'transparent-layer';
  var windowDisplay = document.getElementsByClassName(selectedClassName);
  windowDisplay[0].style.display = 'none';
  removeEventListenerForWindowResizeParams(selectedClassName);
  unmountComponent('manifest');
}

export function goToTopOfWebPage(){
  window.scrollTo(0,0);
}

export function addActiveClassToClassList(id, targetClass, className){
  var classList = document.getElementsByClassName(targetClass);
  if(classList.length>0){
    for(var a= 0; a < classList.length; a++){
      var classListId = parseInt(classList[a].id);
      if(classListId===id){
        classList[a].classList.add(className)
      }else{
        classList[a].classList.remove(className)
      }
    }
  }
}

export function removeClassNameFromClassList(targetClass, className){
  var classList = document.getElementsByClassName(targetClass);
  if(classList != undefined && classList.length>0){
    for(var a=0; a<classList.length;a++){
        classList[a].classList.remove(className);
    }
  }
}
