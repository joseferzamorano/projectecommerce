import React from 'react'
import * as utils from '../../../../frameworks/globals/Utils.js';

export default class ButtonCloseWindow extends React.Component{
  constructor(props){
    super(props);
    this.closeWebPage = this.closeWebPage.bind(this);
  }

  closeWebPage(){
    utils.closeWebPage();
  }
  
  render(){
    return (<div>
              <div className="button-close-window">
                <span className="button-close-window-text" onClick={this.closeWebPage}>&times;
                </span>
              </div>
           </div>)
  }
}
