import React from 'react';

export default class Drawings extends React.Component{

    render(){
        return(
            <div className="svg-container">
        			<svg className="svg1">
        			   <path className="wave-line" d="m 0 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="3"/>

        	  		   <path className="wave-line" d="m 20 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="2.5"/>

                   		<path className="wave-line" d="m 40 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="2"/>

                   		<path className="wave-line" d="m 60 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="1.5"/>

                   		<path className="wave-line" d="m 80 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="1"/>

                   		<path className="wave-line" d="m 100 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="0.5"/>

                   		<path className="wave-line" d="m 120 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="0.2"/>

                   		<path className="wave-line" d="m 140 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="0.2"/>

        	  		   <path className="wave-line" d="m 160 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="0.5"/>

                   		<path className="wave-line" d="m 180 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="1"/>

                   		<path className="wave-line" d="m 200 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="1.5"/>

                   		<path className="wave-line" d="m 220 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="2"/>

                   		<path className="wave-line" d="m 240 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="2.5"/>

                   		<path className="wave-line" d="m 260 80
        			   c 340 -80, 680 -20, 820 -5 s 450  10,1080 -180 "
                   		stroke="white" fill="transparent"
                   		strokeWidth="3"/>
        			</svg>
        		</div>
        )
    }
}
