import React from 'react';
import Logo from './Logo.jsx';
import NavigationBar from './NavigationBar.jsx';
import Membership from './Membership.jsx';
import MobileButton from './MobileButton.jsx';
import Caroussel from './Caroussel.jsx';
import MobileNavigationBar from './MobileNavigationBar.jsx';
import Title from './Title.jsx';
import Drawings from './Drawings.jsx';

export default class Header extends React.Component{
    constructor(props){
      super(props);
    }

  render(){
    return(

  	    <header className="container-flex-header">
          <Caroussel/>
          <div className="container-flex-nav">
              <Logo/>
              <NavigationBar list = {this.props} />
              <Membership/>
              <MobileButton/>
              <MobileNavigationBar list = {this.props} />
              <Title/>
          </div>
          <Drawings/>
        </header>
    )
  }
}
