import React from 'react';

export default class Membership extends React.Component{

    render(){
        return(
            <div>
              <div className="flex-membership">
                <div className="search-container">
                  <div className="search-box"><p>Search</p></div>
                  <div className="search-button"><p>Go</p></div>
                </div>
                <div className="info-nav"><p>contact@flowershop.com</p></div>
                <div className="info-cart"><p>CART: 0 </p></div>
                <div className="membership">Login / Register</div>
              </div>
            </div>)
    }
}
