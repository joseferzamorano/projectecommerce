import React from 'react';

import * as utils from '../../../../../globals/utils.js';
import store from '../../../../../store/index.js';
import {setProductStatusId, removeProductStatusId} from '../../../../../actions/actionsCreators.js';

export default class MobileNavigationBar extends React.Component{
    constructor(props){
      super(props);
      this.toggleStatus = this.toggleStatus.bind(this);
    }

    componentDidUpdate(prevProps,prevState){
      var currentState = store.getState();
      var currentStatusId = currentState.status.statusId;
      if(currentStatusId !== -1){
        utils.addActiveClassToClassList(currentStatusId,'mobile-nav-item','status-active');
      }
      if(currentStatusId === -1){
        utils.removeClassNameFromClassList('mobile-nav-item','status-active')
      }
    }

    toggleStatus(event){
      var stateInfo = store.getState();
      var currentStatusId  =(typeof(stateInfo.status) === 'number') ? stateInfo.status: stateInfo.status.statusId;
      var id = parseInt(event.target.id);
      event.stopPropagation();

      if(id===currentStatusId){
        store.dispatch(removeProductStatusId());
        utils.removeClassNameFromClassList('mobile-nav-item','status-active');
      }

      if(id !==currentStatusId){
        store.dispatch(setProductStatusId(id));
        utils.addActiveClassToClassList(id,'mobile-nav-item','status-active');
      }
    }

    render(){
      var statuses =(this.props.list.statuses !== undefined) ?
          this.props.list.statuses.map((item) => {
             return <li key={item.StatusId}
                        id={item.StatusId}
                        className="mobile-nav-item"
                        onClick={this.toggleStatus}> {item.Name} </li>;
          }) :"not loaded";
        return(
            <div className = "mobile-navigation-section">
                <ul id="mobile-nav" className="mobile-nav-container">
                     {statuses}
                </ul>
            </div>
        )
    }
}
