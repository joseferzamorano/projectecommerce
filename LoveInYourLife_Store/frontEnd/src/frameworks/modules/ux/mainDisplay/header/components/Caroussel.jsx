import React from 'react';

export default class Caroussel extends React.Component{

    render(){
        return(
          <section className="hero-slider">
          		<div className="hero-carousel"></div>
          		<div className="chevron-left"></div>
          		<div className="chevron-right"></div>
          	</section>
          )
    }
}
