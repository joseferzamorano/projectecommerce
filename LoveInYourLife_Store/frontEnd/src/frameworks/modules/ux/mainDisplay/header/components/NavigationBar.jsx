import React from 'react';
import * as utils from '../../../../../globals/utils.js';
import store from '../../../../../store/index.js';
import {setProductStatusId, removeProductStatusId} from '../../../../../actions/actionsCreators.js';

export default class NavigationBar extends React.Component{
    constructor(props){
      super(props);
       this.getSelectedStatus = this.getSelectedStatus.bind(this);
    }

    componentDidUpdate(prevProps,prevState){
      var currentState = store.getState();
      var currentStatusId = currentState.status.statusId;
      if(currentStatusId !== -1){
        utils.addActiveClassToClassList(currentStatusId,'status-li','status-active');
      }
      if(currentStatusId === -1){
        utils.removeClassNameFromClassList('status-li','status-active')
      }
    }

    getSelectedStatus(event){
      var stateInfo = store.getState();
      var currentStatusId  =(typeof(stateInfo.status) === 'number') ? stateInfo.status: stateInfo.status.statusId;
      var id = parseInt(event.target.id);
      event.stopPropagation();
      if(id===currentStatusId){
        store.dispatch(removeProductStatusId());
        utils.removeClassNameFromClassList('status-li','status-active')
      }
      if(id !==currentStatusId){
        store.dispatch(setProductStatusId(id));
        utils.addActiveClassToClassList(id,'status-li','status-active');
      }
    }

    render(){
      var statuses =(this.props.list.statuses !== undefined) ?
                    this.props.list.statuses.map((item)=>{
                       return <li key={item.StatusId}
                                  id={item.StatusId}
                                  className={'status-li'}
                                  onClick={this.getSelectedStatus}> {item.Name} </li>;
                    }) :"";
      return(<div>
                <ul className="flex-navigation">
                  {statuses}
                </ul>
            </div>);
    }

}
