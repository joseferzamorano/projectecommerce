import React from 'react';
import * as utils from '../../../../../../frameworks/globals/Utils.js';

export default class NavigationFooter extends React.Component{
    constructor(props){
      super(props);
      this.displayWebPage = this.displayWebPage.bind(this);
      this.closeWebPage = this.closeWebPage.bind(this);
    }

    displayWebPage(event){
      var selectedClassName = 'transparent-layer';
      utils.addEventListenerForWindowResizeParams(selectedClassName);
      var windowDisplay = document.getElementsByClassName(selectedClassName);
      windowDisplay[0].style.display = 'block';
      windowDisplay[0].style.background = 'white';

      utils.handleDivResizeParams(selectedClassName);
      var id = event.target.id;
      utils.getSelectedComponent(id);
    }

    closeWebPage(){
      utils.closeWebPage();
      utils.goToTopOfWebPage();
    }

    render(){
      return(<div>
                  <div className="footer-elements-ul">
                    <div className="footer-element-li" id="home" onClick={this.closeWebPage}>Home</div>
                    <div className="footer-element-li" id="about_us" onClick={this.displayWebPage}>About Us</div>
                    <div className="footer-element-li" id="t_and_c" onClick={this.displayWebPage}>Terms And Conditions</div>
                    <div className="footer-element-li" id="s_and_r" onClick={this.displayWebPage}>Shippings And Returns</div>
                    <div className="footer-element-li" id="privacy_policy" onClick={this.displayWebPage}>Privacy Policy</div>
                    <div className="footer-element-li" id="security" onClick={this.displayWebPage}>Security</div>
                  </div>
            </div>)
    }
}
