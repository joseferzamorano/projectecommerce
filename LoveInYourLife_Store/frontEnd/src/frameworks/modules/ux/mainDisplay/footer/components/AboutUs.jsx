import React from 'react';
import ButtonCloseWindow from '../../../../../modules/common/components/ButtonCloseWindow.jsx';
import NavigationFooter from './NavigationFooter.jsx';


export default class AboutUs extends React.Component{

  render(){
    return(<div>
              <div className="about-us-background"></div>
                <div className="about-us-container">
                  <div className="about-us-page">
                    <ButtonCloseWindow/>
                    <div><NavigationFooter/></div>
                      <br/><br/><br/>
                      <h2>ABOUT US</h2>

                      <p>We are an Australian based online jewellery store.
                      Our website houses an impeccable blend of iconic designs.
                      Excellent customer service is the secret to our success
                      and you can have confidence that the highest standards of service
                      and integrity will be maintained in our dealings with you.</p>

                      <p>From this website you can see our gorgeous variety, create a wish list,
                      and shop for numerous styles creating a unique personal statement for you.
                      All our orders will ship within 24 -48 hours (subject to availability).
                      </p>
                      <p>
                      If you have a question, please don’t hesitate to ask via the Contact Us page.
                      </p>
                      <p>
                      We hope you enjoy our wonderful products.
                      </p>
                  </div>
                </div>
          </div>)
  }
}
