import React from 'react';
import NavigationFooter from './NavigationFooter.jsx';

export default class Footer extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return(<div className="footer-section">
              <NavigationFooter/>
          </div>)
    }
}
