import React from 'react';

export default class TransparentBackgroundPage extends React.Component{
  constructor(props){
    super(props);
  }
  render(){
    return(
      <div>
        <div id="transparent-layer-id" className="transparent-layer"></div>
      </div>
    );
  }
}
