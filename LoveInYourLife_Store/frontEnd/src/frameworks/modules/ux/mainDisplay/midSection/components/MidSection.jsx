import React from 'react';
import * as utils from '../../../../../../frameworks/globals/Utils.js';

import store from '../../../../../store/index.js';
import {setProductStatusId, removeProductStatusId} from '../../../../../actions/actionsCreators.js';
import {setProductCategoryId, removeProductCategoryId} from '../../../../../actions/actionsCreators.js';

export default class MidSection extends React.Component{
  constructor(props){
    super(props);
    this.getSelectedCategory = this.getSelectedCategory.bind(this);
    this.getProductsForDisplay = this.getProductsForDisplay.bind(this);
    this.getFilteredProductsForDisplay = this.getFilteredProductsForDisplay.bind(this);
    this.getSelectedStatusName = this.getSelectedStatusName.bind(this);
    this.displayAllProducts = this.displayAllProducts.bind(this);
    this.displaySingleProduct = this.displaySingleProduct.bind(this);
    this.getBrowserCurrentSize = this.getBrowserCurrentSize.bind(this);
    this.handleWindowResize = this.handleWindowResize.bind(this);
    this.getCurrentCategoryId = this.getCurrentCategoryId.bind(this);
    this.getCurrentStatusId = this.getCurrentStatusId.bind(this);
  }

  getSelectedCategory(event){
      var id = parseInt(event.target.id);
      var currentCategoryId = this.getCurrentCategoryId();
      if(currentCategoryId===id){
        store.dispatch(removeProductCategoryId());
        utils.removeClassNameFromClassList('category-item','category-active');
      }
      if(currentCategoryId !== id){
        store.dispatch(setProductCategoryId(id));
        utils.addActiveClassToClassList(id,'category-item','category-active')
      }
  }

  getProductsForDisplay(){
    var products = (this.props.products !== undefined) ?
                          this.props.products: "";
    return products;
  }


  getFilteredProductsForDisplay(filterStatusId, filterCategoryId, productsLoaded){
    var statusIdInteger = parseInt(filterStatusId);
    var categoryIdInteger = parseInt(filterCategoryId);
    var productsFilteredByStatus=[]
    for(var a = 0;a<productsLoaded.length ;a++){
       if(productsLoaded[a].StatusId === statusIdInteger){
           productsFilteredByStatus.push(productsLoaded[a])
       }
    }

    productsFilteredByStatus = (statusIdInteger=== -1)
                              ? productsLoaded: productsFilteredByStatus;

    var productsFilteredByCategory = [];
    for(var b =0; b<productsFilteredByStatus.length ; b++){
       if(productsFilteredByStatus[b].CategoryId === categoryIdInteger){
         productsFilteredByCategory.push(productsFilteredByStatus[b])
       }
    }
    productsFilteredByCategory = (categoryIdInteger === -1)?  productsFilteredByStatus : productsFilteredByCategory;
    return productsFilteredByCategory;
  }
  getSelectedStatusName(id){
    var statuses = this.props.statuses;
    for(var a=0; a<statuses.length; a++){
      if(statuses[a].StatusId===id){
        return statuses[a].Name;
      }
    }
    return '';
  }
  displayAllProducts(){
    var displayAllCategories = -1;
    var displayAllStatuses = -1;
    var currentStateInfo = store.getState();
    var currentCategoryId = this.getCurrentCategoryId();
    var currentStatusId = this.getCurrentStatusId();
      if(currentCategoryId === displayAllCategories &&
        currentStatusId === displayAllStatuses){
      return;
    }
    store.dispatch(removeProductStatusId());
    store.dispatch(removeProductCategoryId());
    utils.removeClassNameFromClassList('mobile-nav-item','status-active');
    utils.removeClassNameFromClassList('status-li','status-active');
    utils.removeClassNameFromClassList('category-item','category-active');
    var currentState = store.getState();
  }

  getSelectedProduct(id){
    var allProducts = this.getProductsForDisplay();
    for(var a =0; a<allProducts.length; a++){
      if(allProducts[a].ProductId ===id){
        return allProducts[a];
      }
    }
  }

  displaySingleProduct(event){
    utils.addEventListenerForWindowResizeParams('transparent-layer');
    var id = parseInt(event.target.id);
    var item = event.target.item;
    var singleProduct= this.getSelectedProduct(id);
    utils.setSingleProductObject(singleProduct);
    var windowDisplay = document.getElementsByClassName('transparent-layer');
    windowDisplay[0].style.display = 'block';
    windowDisplay[0].style.background = 'black';
    utils.handleDivResizeParams('transparent-layer');
    event.stopPropagation();
  }

  handleWindowResize(){
  var windowDisplay = document.getElementsByClassName('transparent-layer');
  var currentSize = this.getBrowserCurrentSize();
    windowDisplay[0].style.height = ""+currentSize.height+"px";
    windowDisplay[0].style.width = ""+currentSize.width+"px";
  }

  getBrowserCurrentSize(){
    var size = {
      width: (window.innerWidth > document.body.clientWidth)? window.innerWidth : document.body.clientWidth,
      height: (window.innerHeight > document.body.clientHeight)? window.innerHeight : document.body.clientHeight
    }
    return size;
  }

  getCurrentCategoryId(){
    var currentStateInfo = store.getState();
    var currentCategoryId = typeof(currentStateInfo.category)==="number"? currentStateInfo.category: currentStateInfo.category.categoryId;
    return currentCategoryId;
  }

  getCurrentStatusId(){
    var currentStateInfo = store.getState();
    var currentStatusId = typeof(currentStateInfo.status)==="number"? currentStateInfo.status: currentStateInfo.status.statusId;
    return currentStatusId;
  }
  render(){
    var categories =(this.props.categories !== undefined) ?
                  this.props.categories.map((item)=>{
                     return <div key={item.CategoryId}
                                 id ={item.CategoryId}
                                 className="category-item"
                                 onClick={this.getSelectedCategory}> {item.Name} </div>;
                  }) :"Not Loaded";

    var productsForDisplay = this.getProductsForDisplay();
    var statusId = this.getCurrentStatusId();
    var categoryId = this.getCurrentCategoryId();
    var selectedProducts = (this.props.products !== undefined) ? this.getFilteredProductsForDisplay(statusId, categoryId ,productsForDisplay):"";
    var productsLoaded  = (this.props.products !== undefined) ?
                           selectedProducts.map((item)=>{
                           return <div key={item.ProductId} className="card" id={item.ProductId}>
                                    <div className="button-buy-now">Buy Now</div>
                                    <div className="product-container" id={item.ProductId} onClick={this.displaySingleProduct}>
                                        <div className="price-container" id={item.ProductId} onClick={this.displaySingleProduct}></div>
                                        <h3 className="product-price" id={item.ProductId} item={item}> ${item.Price} </h3>
                                        <img src={'./src/assets/products/' + item.ImageHref1} id={item.ProductId} className="product-image"/>
                                        <div className="category-text-container" id={item.ProductId} onClick={this.displaySingleProduct}></div>
                                        <span className='category-text' id={item.ProductId}> {this.getSelectedStatusName(item.StatusId)}</span>
                                    </div>
                                  </div>
                          }) : "Not loaded";
    return(<div className="mid-section">
        <div className="container-flex-categories">
          {categories}
          <div className="category-item" onClick={this.displayAllProducts}> Show All </div>
        </div>
        <div className="container-store">
          <div className="container-products">
             {productsLoaded}
          </div>
        </div>
      </div>)
  }
}
