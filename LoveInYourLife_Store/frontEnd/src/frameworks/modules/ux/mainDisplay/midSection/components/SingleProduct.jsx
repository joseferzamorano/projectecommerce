import React from 'react';
import * as utils from '../../../../../../frameworks/globals/Utils.js';

import store from '../../../../../store/index.js';
import {setProductDisplay} from '../../../../../actions/actionsCreators.js';


export default class SingleProduct extends React.Component{
  constructor(props){
    super(props);
    this.displayStore = this.displayStore.bind(this);
    this.identifyTarget = this.identifyTarget.bind(this);
  }

  componentDidMount() {
      document.addEventListener("click", this.identifyTarget);
          var singleProductConfig = utils.getSingleProductoObject();
          var objectIsEmpty =utils.isEmpty(singleProductConfig);
          if(objectIsEmpty===false){
            store.dispatch(setProductDisplay(true));
          }
  }

  componentWillUnmount() {
    document.removeEventListener("click", this.identifyTarget);
  }

  identifyTarget(event){
    var className = event.target.className;
    if(className === 'product-image' ||
       className === 'rotated-text' ||
       className === 'price-container' ||
       className === 'product-price'
     ){
       var singleProductConfig = utils.getSingleProductoObject();
       var objectIsEmpty =utils.isEmpty(singleProductConfig);
       if(objectIsEmpty===false){
         store.dispatch(setProductDisplay(true));
       }
    }
    if(className.includes('transparent-layer') || className.includes('single-product-card')){
      this.displayStore();
    }
    event.stopPropagation();
  }

  displayStore(){
    utils.removeEventListenerForWindowResizeParams('transparent-layer');
    store.dispatch(setProductDisplay(false));
    var windowDisplay = document.getElementsByClassName('transparent-layer');
    windowDisplay[0].style.display = 'none';

  }

  render(){
    var stateInfo = store.getState();
    var currentStateDisplay  =(typeof(stateInfo.display) === 'boolean') ? stateInfo.display: stateInfo.display.displayproduct;
    var displayProduct = (currentStateDisplay=== true)? "single-product-display":"single-product-hide";

    var item = utils.getSingleProductoObject();
    var itemIsEmpty = utils.isEmpty(item);
    var product = (!itemIsEmpty)? (
          <div key={item.ProductId} className={"single-product-card " +displayProduct} id={item.ProductId}>
            <div className="single-product-buy-button">Buy Now</div>
            <div className="single-product-container" id={item.ProductId} >
              <span id={item.ProductId} className="single-product-price-item">$ {item.Price}</span>
              <img src={'./src/assets/products/' + item.ImageHref1} id={item.ProductId} className="single-product-image"/>
              <span id={item.ProductId} className="single-product-name">{item.Name}</span>
              <span id={item.ProductId} data-sku={item.SKU} className="single-product-sku"></span>
              <span id={item.ProductId} data-serial-number={item.SerialNumber}className="single-product-serial-number"></span>
              <span id={item.ProductId} className="single-product-description">{item.Description}</span>
              <div className="back-to-store-button" onClick={this.displayStore}> Back To Store</div>

            </div>
          </div>
    ) : "";
    return(<div>{product}</div>)
  }
}
