import React from 'react';
import Header from './header/components/Header.jsx';
import MidSection from './midSection/components/MidSection.jsx';
import SingleProduct from './midSection/components/SingleProduct.jsx';
import Footer from './footer/components/Footer.jsx';

//Different Ways of importing a JSON FILE BEGIN=======
import * as jsonFile from '../../../../../../frontEnd/json-response/jsonfile.json';
var json = require('../../../../../../frontEnd/json-response/jsonfile.json');
//Different Ways of importing a JSON FILE END=========

import store from '../../../store/index.js';
import {businessStoreSetDataLoad, businesStoreSetProductLoad, setProductStatusId} from '../../../actions/actionsCreators.js';

export default class IndexStore extends React.Component{
  constructor(props){
    super(props);
    this.isDev = true;
    this.getProducts = this.getProducts.bind(this);
    this.getJson = this.getJson.bind(this);
    this.getSelectedStatusMaster = this.getSelectedStatusMaster.bind(this);
    this.loadData = this.loadData.bind(this);

    this.loadData();
  }

  componentDidMount(){
    this.loadData();
  }

  loadData(){
    var result = (this.isDev)? this.getJson(): this.getProducts();
  }

  getJson(){
    var jsonData = jsonFile;
    var currentState = store.getState();
    if(currentState.businessStore.dataLoaded===false){
      store.dispatch(businessStoreSetDataLoad(true));
      store.dispatch(businesStoreSetProductLoad(jsonData));
    }
    return jsonData;
  }

  getProducts(){
    var currentState = store.getState();
    console.log('IndexStore-getProducts-AJAX Begin');
    $.ajax ({
    method: "GET",
    async: true,
    crossDomain: true,
    url: "http://localhost:5080/api/storedisplay/",
    contentType: "application/json; charset=utf-8",
     dataType: "json",
    success: function (data) {
        console.log ('AJAX success Data:',data);
        if(currentState.businessStore.dataLoaded === false){
          store.dispatch(businessStoreSetDataLoad(true));
          store.dispatch(businesStoreSetProductLoad(data));
        }
    },
    failure: function (data) {
            console.log('AJAX Failure Data',data.responseText);
    }, //End of AJAX failure function
    error: function (error) {
            console.log ('AJAX Error Data:',error);
    }
  });
  console.log('IndexStore-getProducts-AJAX END');
  }

  getSelectedStatusMaster(id){
      store.dispatch(setProductStatusId(id));
  }

  render(){
    var currentState = store.getState();
    var currentStatusId = typeof(currentState.Status) ==='number'? currentState.status : currentState.status.statusId;

    return(
      <div>
        <Header statuses = {currentState.businessStore.infoProducts[2]}
                selectedStatusMaster={this.getSelectedStatusMaster}
        />
        <MidSection products = {currentState.businessStore.infoProducts[0]}
                    categories={currentState.businessStore.infoProducts[1]}
                    statuses = {currentState.businessStore.infoProducts[2]}
                    selectedStatusId={currentStatusId}
        />
        <Footer />
      </div>
    )
  }
}
