import initialState from './initialState.js';

export default function productStatusReducer(state= initialState.status.statusId, action){
  switch(action.type){
    case "SET_PRODUCT_STATUS_ID":
      return {...state,
          statusId: action.statusId
      }

    case "REMOVE_PRODUCT_STATUS_ID":
      return{ ...state,
          statusId: action.statusId
      }

    default:
    return state;
  }
}
