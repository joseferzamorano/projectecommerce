import {combineReducers} from 'redux';

import productStatusReducer from './productStatusReducer.js';
import productDisplayReducer from './productDisplayReducer.js';
import productCategoryReducer from './productCategoryReducer.js';
import businessStoreReducer from './businessStoreReducer.js';

const rootReducer = combineReducers({
  status: productStatusReducer,
  display: productDisplayReducer,
  category: productCategoryReducer,
  businessStore: businessStoreReducer
})
export default rootReducer;
