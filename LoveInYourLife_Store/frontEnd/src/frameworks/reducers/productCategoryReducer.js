import initialState from './initialState.js';

export default function productCategoryReducer(state= initialState.category.categoryId, action){
      switch(action.type){
        case "SET_PRODUCT_CATEGORY_ID":
            return { ...state,
              categoryId: action.categoryId
            }

        case "REMOVE_PRODUCT_CATEGORY_ID":
            return { ...state,
              categoryId: -1
            }

        default:
            return state;
      }
}
