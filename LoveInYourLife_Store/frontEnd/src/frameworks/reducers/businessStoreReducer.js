import initialState from './initialState';


export default function businessStoreReducer(state = initialState.businessStore,action){

  switch(action.type){

    case "BUSINESS_STORE_SET_DATA_LOAD":
        return { ...state,
          dataLoaded: action.dataLoaded

        }
    case "BUSINESS_STORE_SET_PRODUCT_LOAD":
        return { ...state,
          infoProducts: action.infoProducts
        }
        
    default :
        return state;
  }
}
