import initialState from './initialState.js';

export default function productDisplayReducer(state = initialState.display.displayProduct, action){
    switch(action.type){
      case "SET_PRODUCT_DISPLAY":
        return {
          ...state,
          displayproduct: action.displayProduct
        }

      default:
        return state;
    }
}
