const initialState ={
  status:{
    statusId: -1,
  },
  display:{
    displayProduct:false,
  },
  category:{
    categoryId:-1,
  },
  businessStore:{
    dataLoaded: false,
    infoProducts:[],
  }
}

export default initialState;
