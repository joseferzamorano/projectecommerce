using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogic.Interfaces;
using Store.DataAccess.DataModels;

namespace Store.WebServices.Controllers
{

    [Route("api/status")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private readonly IGenericService<Status> _genericEntity;
        public StatusController (IGenericService<Status> genericEntity)
        {
            _genericEntity = genericEntity;
        }
          // GET api/status
        [HttpGet]
        public ActionResult<IEnumerable<Status>> GetAll()
        {
            var result = _genericEntity.GetAllTEntities();
            return Ok(result);
        }

          // GET api/status/5
        [HttpGet("{id}")]
        public ActionResult GetSingle(int id)
        {
            var result = _genericEntity.GetSingleTEntityById(id);
            if(result == null)
            {
                return NotFound("The category record couldn't be found.");
            }
            return Ok(result);
        }

        // POST api/status
        [HttpPost]
        public IActionResult Post([FromBody] Status entity)
        {
            if(entity == null)
            {
                return BadRequest("category is Null.");
            }

           var result = _genericEntity.CreateTEntity(entity);
           return Ok($"{result} -created");
        }

        // PUT api/status/5
        [HttpPut]
        public IActionResult UpdateStatus([FromBody] Status entity)
        {
             if(entity == null)
            {
                return BadRequest("Category is null");
            }
            var result =_genericEntity.UpdateTEntity(entity);
            return Ok($"{result} -Updated");
        }

        // DELETE api/status/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
           var result = _genericEntity.DeleteTEntity(id);
           return Ok($"{result} -Deleted");
        }
    }
}
