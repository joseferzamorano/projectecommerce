using System;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Store.DataAccess.DataModels;
using Store.BusinessLogic.Interfaces;
using System.Linq;
using Store.Common.ViewModels;
using System.Threading.Tasks;

namespace Store.WebServices.Controllers
{
    [Route("api/storedisplay")]
    [ApiController]
    public class StoreDisplayController : ControllerBase
    {
        private readonly IStoreServices _storeServices;

        private readonly ICategoryServices _categoryServices;
        private readonly IGenericService<Status> _statusService;
        public StoreDisplayController(IStoreServices storeServices,ICategoryServices categoryServices,IGenericService<Status> statusService)
        {
            _storeServices = storeServices;
            _categoryServices = categoryServices;
            _statusService = statusService;
        }

        // GET api/storedisplay
        [HttpGet]
         public async Task<ActionResult<string>> GetAllItemsForDisplayAsync()
        {
           var result = await _storeServices.GetAllStoreComponentsAsync();
            return Ok(result);
        }
    }
}
