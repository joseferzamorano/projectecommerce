
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogic.Interfaces;
using Store.DataAccess.DataModels;
using Store.DataAccess.Interfaces;

namespace Store.WebServices.Controllers
{
    [Route("api/category")]
    [ApiController]
    public class CategoryController: ControllerBase
    {
        ICategoryServices _categoryServices;
        public CategoryController(ICategoryServices categoryServices)
        {
            _categoryServices = categoryServices;
        }


        // GET api/category
        [HttpGet]
        public ActionResult<IEnumerable<Category>> GetAll()
        {
            var result = _categoryServices.GetAll();
            return Ok(result);
        }

        // GET api/category/5
        [HttpGet("{id}")]
        public ActionResult GetSingle(int id)
        {
            var result = _categoryServices.GetBySelectedId(id);
            if(result == null)
            {
                return NotFound("The category record couldn't be found.");
            }
            return Ok(result);
        }

        // POST api/category
        [HttpPost]
        public IActionResult Post([FromBody] Category category)
        {
            if(category == null)
            {
                return BadRequest("category is Null.");
            }

           var result = _categoryServices.CreateCategory(category);

            return Ok($"{result} -created");
        }

        // PUT api/category/5
        [HttpPut]
        public IActionResult UpdateCategory([FromBody] Category category)
        {
             if(category == null)
            {
                return BadRequest("Category is null");
            }
            var result =_categoryServices.UpdateCategory(category);
            return Ok($"{result} -Updated");
        }

        // DELETE api/category/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
           var result = _categoryServices.DeleteCategory(id);
           return Ok($"{result} -Deleted");
        }
    }
}
