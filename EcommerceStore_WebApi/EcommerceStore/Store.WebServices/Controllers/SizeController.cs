using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Store.BusinessLogic.Interfaces;
using Store.DataAccess.DataModels;

namespace Store.WebServices.Controllers
{
    [Route("api/size")]
    [ApiController]
    public class SizeController : ControllerBase
    {
        private readonly IGenericService<Size> _genericEntity;
        public SizeController (IGenericService<Size> genericEntity)
        {
            _genericEntity = genericEntity;
        }

        // GET api/size
        [HttpGet]
        public ActionResult<IEnumerable<Size>> GetAll()
        {
            var result = _genericEntity.GetAllTEntities();
            return Ok(result);
        }

         // GET api/size/5
        [HttpGet("{id}")]
        public ActionResult GetSingle<T>(int id)
        {
            var result = _genericEntity.GetSingleTEntityById(id);
            if(result == null)
            {
                return NotFound("The size record couldn't be found.");
            }
            return Ok(result);
        }

        // POST api/size
        [HttpPost]
        public IActionResult Post([FromBody] Size entity)
        {
            if(entity == null)
            {
                return BadRequest("size is Null.");
            }

           var result = _genericEntity.CreateTEntity (entity);

            return Ok($"{result} -created");
        }

        // PUT api/size/5
        [HttpPut]
        public IActionResult UpdateSize([FromBody] Size entity)
        {
             if(entity == null)
            {
                return BadRequest("Size is null");
            }
            var result =  _genericEntity.UpdateTEntity(entity);
            return Ok($"{result} -Updated");
        }

        // DELETE api/size/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
           var result = _genericEntity.DeleteTEntity(id);
           return Ok($"{result} -Deleted");
        }
    }
}
