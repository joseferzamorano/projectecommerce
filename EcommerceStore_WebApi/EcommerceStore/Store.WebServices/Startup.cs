﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using Store.BusinessLogic.Interfaces;
using Store.BusinessLogic.Services;
using Store.DataAccess.DataModels;
using Store.DataAccess.EFContext;
using Store.DataAccess.Interfaces;
using Store.DataAccess.Repositories;

namespace Store.WebServices
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
             //Database Configuration
            var connectionString = Configuration["ConnectionString:DefaultDatabase"];
            services.AddDbContext<StoreContext>(opts=>opts.UseSqlServer(connectionString));
            services.AddScoped<DbContext, StoreContext>();


            //Interfaces and Dependency Injection
         
            services.AddScoped<ICategoryServices, CategoryServices>();
            services.AddScoped<IGenericRepository<Category>, GenericRepository<Category>>(); 
            services.AddScoped<IStoreServices,StoreServices>();        
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient(typeof(IGenericService<>), typeof(GenericEntityService<>));
          
            // Add service and create Policy with options
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials() );
            });
            // services.AddMvc(x => {
            //     x.Filters.Add(new RequireHttpsAttribute());
            // }); 
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
            .AddJsonOptions(x => x.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseExceptionHandler("/Error");
                //app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            //global policy - assign here or on each controller
            app.UseCors("CorsPolicy");

            
            // IMPORTANT: Make sure UseCors() is called BEFORE this
            // app.UseMvc(routes =>
            // {
            //     routes.MapRoute(
            //         name: "default",
            //         template: "{controller=Home}/{action=Index}/{id?}");
            // });
            
            app.UseMvc();
        }
    }
}
