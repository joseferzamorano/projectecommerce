using System.Collections.Generic;
using Store.DataAccess.DataModels;

namespace Store.Common.ViewModels
{
    public class ProductsComponents
    {
        public List<Product> Products { get; set; }
        public List<Category> Categories { get; set; }
        public List<Status> Statuses { get; set; }
       
    }
}