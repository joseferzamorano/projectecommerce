using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Store.BusinessLogic.Interfaces;
using Store.DataAccess.DataModels;
using Store.DataAccess.EFContext;
using Store.DataAccess.Interfaces;
using Store.DataAccess.Repositories;

namespace Store.BusinessLogic.Services
{
    public class CategoryServices : GenericRepository<Category>, ICategoryServices
    {
        private readonly IUnitOfWork _uow;
        private readonly StoreContext _context;
        public CategoryServices(IUnitOfWork unitOfWork, StoreContext context): base(unitOfWork,context)
        {
            _uow = unitOfWork;
            _context = context;
        }

        public IEnumerable<Category> GetAllCategories()
        {
            var categories = GetAll();
            return categories;
        }

        public int CreateCategory(Category category)
        {
            _uow.Context.Add(category);
            var createdQty =_uow.Commit();
            return createdQty;
        }

        public int UpdateCategory(Category category)
        {
            Category selectedCategory = GetBySelectedId(category.CategoryId);

            selectedCategory.Name = category.Name;
            selectedCategory.Description = category.Description;
            selectedCategory.IsInactive = category.IsInactive;
            _uow.Context.Update(selectedCategory);
            var committedQty = _uow.Commit();
            return committedQty;
        }

        public int DeleteCategory(int id)
        {
            Category selectedCategory = GetBySelectedId(id);
            _uow.Context.Remove(selectedCategory);
            var result = _uow.Commit();
            return result;
        }

        public IEnumerable<Category> GetActiveCategories()
        {
            var categories =_context.Categories.Where(a =>a.IsInactive ==false);
            return categories;
        }

        public Category GetSingleCategoryById(int id)
        {
            var cat = GetBySelectedId(id);
             return cat;
        }
    }
}
