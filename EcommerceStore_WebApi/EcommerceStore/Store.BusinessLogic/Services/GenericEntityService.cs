using System;
using System.Collections.Generic;
using System.Linq;
using Store.BusinessLogic.Interfaces;
using Store.DataAccess.Interfaces;

namespace Store.BusinessLogic.Services
{
    public class GenericEntityService<T> : IGenericService<T> where T : class
    {
        private readonly IUnitOfWork _uow;

        public GenericEntityService(IUnitOfWork uow){
            _uow = uow;
        }
        public IEnumerable<T> GetAllTEntities()
        {
            return _uow.Context.Set<T>().AsEnumerable<T>();
        }

        public T GetSingleTEntityById(int id)
        {
            var result = _uow.Context.Set<T>().Find(id);
           return result;
        }

        public int CreateTEntity(T TEntity)
        {
            _uow.Context.Set<T>().Add(TEntity);
            var result = _uow.Commit();
            return result;
        }

        public int UpdateTEntity(T TEntity)
        {
             _uow.Context.Set<T>().Attach(TEntity);
            _uow.Context.Entry(TEntity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            var result = _uow.Commit();
            return result;
        }

        public int DeleteTEntity(int id)
        {
            var entity = _uow.Context.Set<T>().Find(id);
            if(entity ==null)
            {
                return 0;
            }
            _uow.Context.Set<T>().Remove(entity);
            var result = _uow.Commit();
            return result;
        }
    }
}