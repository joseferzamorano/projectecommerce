using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Store.BusinessLogic.Interfaces;
using Store.Common.ViewModels;
using Store.DataAccess.DataModels;
using Store.DataAccess.EFContext;
using Store.DataAccess.Interfaces;
using Store.DataAccess.Repositories;


namespace Store.BusinessLogic.Services
{
    public class StoreServices : IStoreServices
    {
        private readonly IUnitOfWork _uow;
        private readonly StoreContext _context;

        public StoreServices(IUnitOfWork unitOfWork, StoreContext context)
        {
            _uow = unitOfWork;
            _context = context;
        }

        public string GetAllStoreComponents()
        {
            var products =  _context.Products.Include(a =>a.ProductSizes).ToList();
            var categories =  _context.Categories.ToList();
            var statuses =  _context.Statuses.ToList();
            var sizes = _context.Sizes.ToList();

            List<object> objects = new List<object>(){products,categories,statuses,sizes};

            string objectsJson = JsonConvert.SerializeObject(objects, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            return objectsJson;
        }

        public async Task<string> GetAllStoreComponentsAsync()
        {
            var products = await _context.Products.Include(a =>a.ProductSizes).ToListAsync();
            var categories = await  _context.Categories.ToListAsync();
            var statuses = await _context.Statuses.ToListAsync();
            var sizes = await _context.Sizes.ToListAsync();

            List<object> objects = new List<object>(){products,categories,statuses,sizes};

            string objectsJson = JsonConvert.SerializeObject(objects, Formatting.None,
                        new JsonSerializerSettings()
                        {
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
            return objectsJson;
        }
    }
}
