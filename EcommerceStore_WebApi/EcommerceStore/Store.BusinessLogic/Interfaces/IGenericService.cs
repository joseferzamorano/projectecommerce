using Store.DataAccess.Interfaces;
using System.Collections.Generic;

namespace Store.BusinessLogic.Interfaces
{
    public interface IGenericService<T>  where T: class
    {
        IEnumerable<T> GetAllTEntities();
        T GetSingleTEntityById(int id);
        int CreateTEntity(T TEntity);
        int UpdateTEntity(T TEntity);
        int DeleteTEntity(int id);
    }
}