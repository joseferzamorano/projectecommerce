using System.Collections.Generic;
using Store.DataAccess.DataModels;
using Store.DataAccess.Interfaces;

namespace Store.BusinessLogic.Interfaces
{
    public interface ICategoryServices : IGenericRepository<Category>
    {  
        IEnumerable<Category> GetAllCategories();
        Category GetSingleCategoryById(int id);
        int CreateCategory(Category category);
        int UpdateCategory(Category category);
        int DeleteCategory(int id);
        IEnumerable<Category> GetActiveCategories();
    }
}