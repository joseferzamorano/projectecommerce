using System.Collections.Generic;
using System.Threading.Tasks;
using Store.DataAccess.DataModels;

namespace Store.BusinessLogic.Interfaces
{
    public interface IStoreServices
    {
         Task<string> GetAllStoreComponentsAsync();
         string GetAllStoreComponents();
    }
}