using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Store.DataAccess.DataModels
{
    [JsonObject(IsReference = true)] 
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CategoryId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsInactive { get; set; }

        [JsonIgnore] 
        [IgnoreDataMember] 
        public virtual ICollection<Product> Product {get; set;}

    }
}