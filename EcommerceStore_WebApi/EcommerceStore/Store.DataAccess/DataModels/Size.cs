using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace Store.DataAccess.DataModels
{
    [JsonObject(IsReference = true)] 
    public class Size
    {  
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int SizeId { get; set; }   
        [Required]
        public string Description { get; set; }
        public bool IsInactive { get; set; }
        [JsonIgnore] 
        [IgnoreDataMember] 
        public ICollection<ProductSize> ProductSizes { get; set; }  
    }
}