using Microsoft.EntityFrameworkCore;
using Store.DataAccess.DataModels;
namespace Store.DataAccess.Configuration
{
    public class ProductSizeConfiguration : IEntityTypeConfiguration<ProductSize>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<ProductSize> builder)
        {
            builder.HasKey(s=> new{s.ProductSizeId});

            builder.HasOne(ss=>ss.Product)
            .WithMany(s=>s.ProductSizes)
            .HasForeignKey(ss=>ss.ProductId);

            builder.HasOne(ss=>ss.Size)
            .WithMany(s=>s.ProductSizes)
            .HasForeignKey(ss=>ss.SizeId);
        }
    }
}