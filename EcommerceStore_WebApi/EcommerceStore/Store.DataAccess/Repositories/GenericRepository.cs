using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Store.DataAccess.EFContext;
using Store.DataAccess.Interfaces;

namespace Store.DataAccess.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;

        public GenericRepository(IUnitOfWork unitOfWork, StoreContext context)
        {
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<T> GetAll()
        {
            var result = _unitOfWork.Context.Set<T>().AsEnumerable<T>();
            return result;
        }

        public T GetBySelectedId(int id)
        {
            var result = _unitOfWork.Context.Set<T>().Find(id);
            return result;
        }

        public IEnumerable<T> Get(Expression<Func<T, bool>> predicate)
        {
           var result = _unitOfWork.Context.Set<T>().Where(predicate).AsEnumerable<T>();
           return result;
        }

        public void Add(T entity)
        {
            var result = _unitOfWork.Context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            _unitOfWork.Context.Set<T>().Attach(entity);
            var result = _unitOfWork.Context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }

        public void Delete(T entity)
        {
            T existing = _unitOfWork.Context.Set<T>().Find(entity);
            if(existing !=null){
                _unitOfWork.Context.Set<T>().Remove(existing);
            }
        }

        public void Delete(int id)
        {
            T existing= _unitOfWork.Context.Set<T>().Find(id);
            if(existing !=null){
                _unitOfWork.Context.Set<T>().Remove(existing);
            }
        }
    }
}