using Microsoft.EntityFrameworkCore;
using Store.DataAccess.Interfaces;

namespace Store.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        public DbContext Context {get;}

        public UnitOfWork(DbContext context )
        {
            Context = context;
        }

        public int Commit()
        {
            var result = Context.SaveChanges();
            return result;
        }

        public void Dispose()
        {
            Context.Dispose();
        }
    }
}