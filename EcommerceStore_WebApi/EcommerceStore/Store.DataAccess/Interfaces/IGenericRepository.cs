using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Store.DataAccess.Interfaces
{
    public interface IGenericRepository<T> where T : class
    {
        IEnumerable<T> GetAll(); 
        T GetBySelectedId(int id);
        IEnumerable<T> Get(Expression<Func<T, bool>> predicate);  
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Delete(int id);              
    }
}