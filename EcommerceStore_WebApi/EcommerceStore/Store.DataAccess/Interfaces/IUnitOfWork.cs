using System;
using Microsoft.EntityFrameworkCore;

namespace Store.DataAccess.Interfaces
{
    public interface IUnitOfWork :IDisposable
    {
         DbContext Context {get; }
         int Commit();
    }
}