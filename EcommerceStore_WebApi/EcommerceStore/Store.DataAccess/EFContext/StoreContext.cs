using Microsoft.EntityFrameworkCore;
using Store.DataAccess.Configuration;
using Store.DataAccess.DataModels;

namespace Store.DataAccess.EFContext
{
    public class StoreContext : DbContext
    {
         public StoreContext(DbContextOptions<StoreContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductSizeConfiguration());
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }

        public DbSet<ProductSize> productSizes { get; set; }
        public DbSet<Size> Sizes { get; set; }

        public DbSet<Status> Statuses { get; set; }
    }
}