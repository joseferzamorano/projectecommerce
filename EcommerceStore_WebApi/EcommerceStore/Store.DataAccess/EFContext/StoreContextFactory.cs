using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Store.DataAccess.EFContext
{
    public class StoreContextFactory: IDesignTimeDbContextFactory<StoreContext>
    {
        public StoreContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<StoreContext>();
             var connectionString = "server=localhost;database=StoreDB;User ID=sa;password=abc123!@#;";
            optionsBuilder.UseSqlServer(connectionString);
            return new StoreContext(optionsBuilder.Options);
        }
    }
}