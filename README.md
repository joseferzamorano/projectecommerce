**Project Ecommerce**

This project has been separated in two folders.

**LoveInYourLife_Store:**

This folder contains the front end section designed and created as a responsive Mobile First website built using React.js, Redux
Javascript, JQuery.Ajax for requests and responses as well as HTML and SASS. Build tool is Gulp.

**EcommerceStore_WebApi:**

This folder contains the server side section designed using DOT NET Core technologies in a Linux environment: C# , Entity Framework for Linux and SQL Server for Linux database.
The project was created using SOLID principles, N-tier layers, Generic repository pattern and Unit of Work pattern.


The Front End has been subdivided in 2 sections.

1. **dist** all the production ready files.
2. **src** all the development files, divided in compartments to easily identify React from Redux functionalities.



## Run the code
The repo does not contain the node_modules folder, so it is necessary to
1. Run `npm install` to reinstall all the packages.

2. Run `gulp watch` afterwards to start the server and run the project locally in your machine.

3. Run `gulp build` to create all the production ready files that will be
placed in the build folder.

##For demo and showcase only.
